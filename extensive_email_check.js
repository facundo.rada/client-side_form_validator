$(document).ready(function(){
	$("#submit_btn").click(function(event) {
		event.preventDefault();
		var formMessages = $('#form-messages');
		var form = $('#contact_form');
		//
		//<<<Section check for valid input>>>
		//
		var proceed = true;
		//Clear the messages
		$(formMessages).html("");
		$(formMessages).removeClass('mail_error');
		$(formMessages).removeClass('mail_success');
		
		//simple validation at client's end
		//loop through each field and we simply change border color to red for invalid fields      
		$("#contact_form input[required=true], #contact_form textarea[required=true]").each(function(){
			$(this).css('border-color','transparent'); //reset border color
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				$(formMessages).removeClass('mail_success');
				$(formMessages).addClass('mail_error');
				$(formMessages).append($(this).attr("placeholder")+" es requerido<br>");
				proceed = false; //set do not proceed flag
			}
			//check invalid email
			var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if($(this).attr("type")=="email" && !email_reg.test($.trim($(this).val()))){
				$(this).css('border-color','red'); //change border color to red  
				$(formMessages).removeClass('mail_success');
				$(formMessages).addClass('mail_error');
				$(formMessages).append("No parece un Email Valido<br>");
				proceed = false; //set do not proceed flag
			}
		});
		//
		//<<<Section Send stuff>>>
		//
		if(proceed){
			var formData = $(form).serialize();
			$.ajax({
				type: 'POST',
				url: $(form).attr('action'),
				data: formData,
				success: function(data){
					if($.trim(data)){
						// 0 - success
						// 1 - mail can't be sent
						// 2 - empty fields
						// 3 - mail field didn't pass regex check
						if(data[0] == "0"){ //Success
							$(formMessages).removeClass('mail_error');
							$(formMessages).addClass('mail_success');
							$(formMessages).html("Su mensaje ha sido enviado :)");
							$("#contact_form input[required=true], #contact_form textarea[required=true]").each(function(){
								$(this).val("");
							});
						} else if(data[0] == "1"){ //mail can't be sent
							$(formMessages).addClass('mail_error');
							$(formMessages).removeClass('mail_success');
							$(formMessages).html('El mensaje no pudo ser enviado :(<br>Intente de nuevo mas tarde.');
						} else if(data[0] == "2"){ //empty fields
							$(formMessages).addClass('mail_error');
							$(formMessages).removeClass('mail_success');
							$(formMessages).html('Debe completar todos los campos');
						} else if(data[0] == "3"){ //mail field didn't pass regex check
							$(formMessages).addClass('mail_error');
							$(formMessages).removeClass('mail_success');
							$(formMessages).html('No parece un Email Valido');
						}
					}
				},
				error: function(data){
					// Make sure that the formMessages div has the 'error' class.
					$(formMessages).removeClass('mail_success');
					$(formMessages).addClass('mail_error');

					// Set the message text.
					if ($.trim(data)) {
						$(formMessages).html(data);
					} else {
						$(formMessages).html('El mensaje no pudo ser enviado :(<br>Intente de nuevo mas tarde.');
					}
				},
				beforeSend: function(){
					// console.log("sending..."); 
					// $("#dataPanel").html("retrieving info..."); //Clear the content
				}
			});
		}
	});
});
